package com.zinovo.govdelivery;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import java.io.IOException;
import java.util.Base64;

public class App
{

    public static void main(String[] args) throws IOException
    {

        String subscriberId   = "";
        String govDelUsername = "";
        String govDelPassword = "";
        String accountCode    = "COTRANRTD";

        String encodedSubscriberId = Base64.getEncoder().encodeToString(subscriberId.getBytes());
        String encodedCredentials  =
                Base64.getEncoder().encodeToString((govDelUsername + ":" + govDelPassword).getBytes());

        System.out.println("Retrieving GovDelivery Subscriber Information for: " + govDelUsername);

        System.out.println(encodedSubscriberId);
        System.out.println(encodedCredentials);

        OkHttpClient client = new OkHttpClient().newBuilder().build();
        Request request = new Request.Builder()
                .url("https://api.govdelivery.com/api/account/" + accountCode + "/subscribers/" + encodedSubscriberId + ".xml")
                .method("GET", null)
                .addHeader("Authorization", "Basic " + encodedCredentials)
                .build();
        try (Response response = client.newCall(request).execute()) {
            System.out.println(response.body().string());
        }
    }

}
